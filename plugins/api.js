import { ApiWrapper } from '~/api/wrapper'

export default function (context, inject) {
  const apiInstance = new ApiWrapper(context.app.$axios, context.app.store)

  inject('api', apiInstance)
}
