import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { Store } from 'vuex'
import { RootState } from '~/store/types'
export * from './types'

export class ApiWrapper {
  private axios: NuxtAxiosInstance
  private store: Store<RootState>

  public readonly check: CheckApi
  public readonly auth: AuthApi

  constructor(axios: NuxtAxiosInstance, store: Store<RootState>) {
    this.axios = axios
    this.store = store
  }
}
