export interface PaginatedResponse<T> {
  limit: number
  offset: number
  data: T[]
}

export interface Response<T> {
  data: T
}

export interface UserInfo {
  id: string | undefined
  email: string | undefined
  type: string | undefined
  first_name: string | undefined
  last_name: string | undefined
  middle_name: string | undefined
  birthday: string | undefined
  phone: string | undefined
  address: string | undefined
  passport: string | undefined
  documents: Array<any>
  orders: Array<any>
}
