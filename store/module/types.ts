import { BuildMutations, BuildActions, BuildGetters } from '../typingSupport'
import { RootState } from '../types'

export interface CheckState {}

interface CheckGettersTypes {}

interface CheckMutationTypes {}

interface CheckActionTypes {}

export type CheckGetters = BuildGetters<
  CheckGettersTypes,
  CheckState,
  RootState
>

export type CheckMutations = BuildMutations<CheckMutationTypes, CheckState>

export type CheckActions = BuildActions<
  CheckActionTypes,
  CheckState,
  CheckGetters,
  CheckMutationTypes,
  RootState
>
