import { CheckState, CheckMutations, CheckGetters, CheckActions } from './types'

export const state = (): CheckState => ({})

export const mutations: CheckMutations = {}

export const getters: CheckGetters = {}

export const actions: CheckActions = {}
