import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { Auth0Client } from '@auth0/auth0-spa-js'
import { ApiWrapper } from '~/api/wrapper'

export type PrebuildGetter<S, R, G extends object, T = any> = (
  state: S,
  getters: G,
  rootState: R,
  rootGetters: any
) => T

export type BuildGetters<T extends object, S, R> = {
  [k in keyof T]: PrebuildGetter<S, R, T, T[k]>
}

export type BuildMutations<T extends object, S> = {
  [k in keyof T]: (state: S, payload: T[k]) => void
}

type FirstParameter<T> = T extends (k: infer P) => any ? P : never
type Returns<T> = T extends (...any) => infer P ? P : never //eslint-disable-line

export type MutationsDescription = { [k: string]: any }
export type ActionsDescription = { [k: string]: (any) => any }

export type GenericCommitable = (
  name: string,
  payload: any,
  options: { root: true }
) => void

type CommitType<Mutations extends MutationsDescription> = <
  Name extends keyof Mutations
>(
  name: Name,
  payload: Mutations[Name],
  options?: undefined
) => void
type DispatchType<Actions extends object> = <
  Name extends keyof Actions,
  Payload extends FirstParameter<Actions[Name]>
>(
  name: Name,
  payload: Payload,
  options?: undefined
) => Returns<Actions[Name]>

export type BuildActions<
  T extends object,
  S,
  G,
  M extends MutationsDescription,
  R
> = {
  [k in keyof T]: (
    this: {
      $axios: NuxtAxiosInstance
      $auth0: Auth0Client
      $api: ApiWrapper
    },
    context: {
      state: S
      rootState: R
      getters: G
      commit: CommitType<M> & GenericCommitable
      dispatch: DispatchType<T> & GenericCommitable
      rootGetters: any
    },
    payload: FirstParameter<T[k]>
  ) => Returns<T[k]>
}
