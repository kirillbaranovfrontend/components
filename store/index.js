import Vuex from 'vuex'
import { modules } from './modules'

const CreateStore = () => {
  return new Vuex.Store({
    modules: modules(),
  })
}

export default CreateStore
