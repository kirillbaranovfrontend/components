import { Modules } from './modules'

type ExtractState<T> = {
  [k in keyof T]: T[k] extends { modules: infer M; state: infer S }
    ? S & ExtractState<M>
    : T[k] extends { modules: infer M }
    ? ExtractState<M>
    : T[k] extends { state: infer S }
    ? S
    : never
}

type ExtractGetters<T> = {
  [k in keyof T]: T[k] extends { modules: infer M; getters: infer S }
    ? S & ExtractGetters<M>
    : T[k] extends { modules: infer M }
    ? ExtractGetters<M>
    : T[k] extends { getters: infer S }
    ? S
    : never
}

type ExtractMutations<T> = {
  [k in keyof T]: T[k] extends { modules: infer M; mutations: infer S }
    ? S & ExtractMutations<M>
    : T[k] extends { modules: infer M }
    ? ExtractMutations<M>
    : T[k] extends { mutations: infer S }
    ? S
    : never
}

type ExtractActions<T> = {
  [k in keyof T]: T[k] extends { modules: infer M; actions: infer S }
    ? S & ExtractActions<M>
    : T[k] extends { modules: infer M }
    ? ExtractActions<M>
    : T[k] extends { actions: infer S }
    ? S
    : never
}

export interface RootState extends ExtractState<Modules> {}

export interface RootGetters extends ExtractGetters<Modules> {}

export interface RootMutations extends ExtractMutations<Modules> {}

export interface RootActions extends ExtractActions<Modules> {}
