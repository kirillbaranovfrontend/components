import * as check from './module'

export const modules = () => ({
  check: {
    namespaced: true,
    state: check.state(),
    getters: check.getters,
    mutations: check.mutations,
    actions: check.actions,
  },
})

export type Modules = typeof modules extends () => infer R ? R : never
