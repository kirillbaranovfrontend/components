import config from 'config'
const API_HOST = config.get('API_HOST')

export default {
  head: {
    title: 'WonderWorld',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  styleResources: {
    scss: ['@/assets/scss/_vars.scss'],
  },
  css: ['@/assets/scss/style.scss'],

  plugins: ['~/plugins/axios.js', '~/plugins/api.js'],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxt/typescript-build',
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    'vue-wait/nuxt',
    ['vue-wait/nuxt', { useVuex: true }],
  ],

  wait: { useVuex: true },

  axios: {},

  proxy: {
    '/api': {
      target: API_HOST,
      pathRewrite: { '^/api(?:/v3){0,1}(?=/)': API_HOST },
      changeOrigin: true,
      hostRewrite: true,
    },
  },

  pwa: {
    manifest: {
      lang: 'ru',
    },
  },

  build: {},
}
